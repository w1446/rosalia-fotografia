const themeTitleTag = document.querySelector('.theme-title');
// Objeto com a chave "Nome do Tema (id)" e "Descrição do Tema"
const themeTitle = {
  'theme-light': 'Tema Cáqui',
  'theme-lighter': 'Tema Claro',
  'theme-dark': 'Tema Escuro',
  'theme-darker': 'Tema Obscuro'
};
// Objeto com a chave "Tema atual" e "Proximo Tema"
const nextTheme = {
  'theme-lighter': 'theme-light',
  'theme-light': 'theme-dark',
  'theme-dark': 'theme-darker',
  'theme-darker': 'theme-lighter'
};

function setTheme(themeName) {
  // Armzaena o nome do tema no localStorage
  localStorage.setItem('theme', themeName);
  // Seta o nome do tema na classe
  document.documentElement.className = themeName;
  // Seta a descrição do tema na tag Title
  themeTitleTag.setAttribute("title", themeTitle[themeName]);
}

function toggleTheme() {
  setTheme(nextTheme[localStorage.getItem('theme')]);
}

(function() {
  // Define um tema inicial para aplicar quando não existe valor no Storage
  let temaInicial = (!localStorage.getItem('theme')) ? 'theme-lighter' : localStorage.getItem('theme');
  setTheme(temaInicial);
})();
