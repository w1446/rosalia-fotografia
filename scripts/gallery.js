let selected = {
  image: document.getElementById('selected-image'),
  index: 0,
  filter: ''
};

const gallery = document.getElementById('gallery');
const popup = document.getElementById('popup-container');
const previous = document.getElementById('btn-previous');
const next = document.getElementById('btn-next');
const close = document.getElementById('btn-close');
const pessoal = document.getElementById('lb-pessoal');
const moda = document.getElementById('lb-moda');
const gestante = document.getElementById('lb-gestante');
const estudio = document.getElementById('lb-estudio');

const sideMenuCheck = document.getElementById('side-menu');
const itemsMenu = document.getElementsByClassName('menu__item');


const range = (start, end, length = end - start) => Array.from({
  length
}, (_, i) => start + i);
const imageIndexes = range(100, 173);
const pessoalIndexes = range(100, 130);
const modaIndexes = range(130, 155);
const gestanteIndexes = range(155, 165);
const estudioIndexes = range(165, 173);


function getActualIndexes() {
  if (selected.filter == pessoal.id)
    return pessoalIndexes;
  else if (selected.filter == moda.id)
    return modaIndexes;
  else if (selected.filter == gestante.id)
    return gestanteIndexes;
  else if (selected.filter == estudio.id)
    return estudioIndexes;
}

function nextImageIndex() {
  let actualIndexes = getActualIndexes();
  selected.index += 1;
  if (actualIndexes.indexOf(selected.index) == -1)
    selected.index = actualIndexes[0];
}

function previousImageIndex() {
  let actualIndexes = getActualIndexes();
  selected.index -= 1;
  if (actualIndexes.indexOf(selected.index) == -1)
    selected.index = actualIndexes[actualIndexes.length-1];
}

const filtrarFotos = ((listaImg, indicesValidos, categoria) => {
  let numPhoto = 0,
    posIniStr = 0,
    posFinStr = 0;

  listaImg.forEach((img, i) => {
    posIniStr = img.src.lastIndexOf('-');
    posFinStr = img.src.lastIndexOf('.');

    if (posIniStr > -1 && posFinStr > -1) {
      numPhoto = parseInt(img.src.substring(posIniStr + 1, posFinStr));

      if (indicesValidos.indexOf(numPhoto) == -1)
        img.setAttribute("hidden", "hidden");
      else
        img.removeAttribute("hidden", "hidden");

      img.classList.remove('image-grid-2-rows');
      img.classList.remove('image-grid-2-cols');

      if (categoria == pessoal.id) {
        if ([100, 103, 108, 111, 112, 117, 116, 122, 123, 124, 125, 156, 166].indexOf(numPhoto) > -1)
          img.classList.add('image-grid-2-rows');
        if ([108, 116, 122, 136, 140, 156, 160].indexOf(numPhoto) > -1)
          img.classList.add('image-grid-2-cols');
      } else {
        if ([130, 142, 155, 165, 169].indexOf(numPhoto) > -1)
          img.classList.add('image-grid-2-rows');
        if ([130, 136, 134, 140, 142, 155, 160, 162].indexOf(numPhoto) > -1)
          img.classList.add('image-grid-2-cols');
      }
    }
  });
})

// Percorre o range de todas as imagens para carregá-las no grid
imageIndexes.forEach((i) => {
  const image = document.createElement('img');
  image.src = `galeria/miniaturas/Photo-${i}.jpg`;
  image.alt = `Ensaio fotográfico por Rosália Benvegnú. Photo ${i}`;
  image.classList.add('gallery__image');
  // Adiciona a classe que fará a imagem ocupar duas linhas e/ou duas linhas
  if ([100, 103, 108, 111, 112, 117, 116, 122, 123, 124, 125, 156, 166].indexOf(i) > -1)
    image.classList.add('image-grid-2-rows');
  if ([116, 108, 122, 136, 140, 156, 160].indexOf(i) > -1)
    image.classList.add('image-grid-2-cols');

  image.addEventListener('click', () => {
    popup.style.transform = `translateY(0)`;
    selected.image.src = `galeria/Photo-${i}.jpg`;
    selected.image.alt = `Ensaio fotográfico por Rosália Benvegnú. Photo ${i}`;
    selected.index = i;
  })

  gallery.appendChild(image);
  selected.filter = pessoal.id; // Armazena para não precisar
});

close.addEventListener('click', () => {
  popup.style.transform = `translateY(-100%)`;
  selected.image.src = '';
})

next.addEventListener('click', () => {
  nextImageIndex();
  selected.image.src = `galeria/Photo-${selected.index}.jpg`;
})

previous.addEventListener('click', () => {
  previousImageIndex();
  selected.image.src = `galeria/Photo-${selected.index}.jpg`;
})

pessoal.addEventListener('click', () => {
  if (selected.filter != pessoal.id)
    filtrarFotos(gallery.childNodes, pessoalIndexes, pessoal.id);
  selected.filter = pessoal.id;
})

moda.addEventListener('click', () => {
  if (selected.filter != moda.id)
    filtrarFotos(gallery.childNodes, modaIndexes, moda.id);
  selected.filter = moda.id;
})

gestante.addEventListener('click', () => {
  if (selected.filter != gestante.id)
    filtrarFotos(gallery.childNodes, gestanteIndexes, gestante.id);
  selected.filter = gestante.id;
})

estudio.addEventListener('click', () => {
  if (selected.filter != estudio.id)
    filtrarFotos(gallery.childNodes, estudioIndexes, estudio.id);
  selected.filter = estudio.id;
})

for (var i = 0; i < itemsMenu.length -1; i++) {
  let itemA = (itemsMenu[i].childNodes[1]); //get tag A
  itemA.addEventListener('click', () => {
    sideMenuCheck.checked = false;
  })
}

filtrarFotos(gallery.childNodes, pessoalIndexes, pessoal.id);
gallery.classList.add("gallery-active");

document.onkeydown = function(e) {
  if (e.keyCode == 27) { // Escape
    if (document.getElementById('popup-container').style.transform == 'translateY(0px)') // Verifica se o popup está aberto
      document.getElementById('btn-close').click(); // Simula o click no Close
  } else if (e.keyCode == 13) { // Enter
    if (document.getElementById('popup-container').style.transform == 'translateY(0px)') // Verifica se o popup está aberto
      document.getElementById('btn-next').click(); // Simula o click no Next
  }
};
