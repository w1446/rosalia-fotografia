# rosalia-fotografia
Um site responsivo e minimalista com o Portfólio fotográfico de Rosália Benvegnú desenvolvido em HTML, CSS e JavaScript.

## Proposta/Objetivo
Esse projeto foi desenvolvido para aperfeiçoar e aprofundar meus estudos em desenvolvimento web.

## Importante
Em desenvolvimento ...

## Ferramentas utilizadas
- Ícones: [Fork Awesome](https://forkaweso.me/Fork-Awesome/icon/)
- Compactador de imagens: [GraphicsMagic](http://www.graphicsmagick.org/)
- Editor de código: [Atom](https://atom.io/) e [NeoVim](https://neovim.io/)
<!-- Loading gif: [acegif](https://acegif.com/)
-->
<!-- ## Navegadores testados
- Firefox
- Google Chrome
- Google Chrome azul (Chromium)
- Google Chrome da MS (Edge)
- Google Chrome vermelho (Opera)
- Google Chrome "privado" (Brave)

*Nas versões desktop e mobile* -->

## Clone
```
git clone https://gitlab.com/w1446/rosalia-fotografia.git
```

## Visite o site em
https://w1446.gitlab.io/rosalia-fotografia/

## Screenshot
![Screenshot do site](./screenshot.png)

## Autor
Daniel Pedrotti
[https://linkedin.com/in/daniel-pedrotti](https://linkedin.com/in/daniel-pedrotti)
[https://instagram.com/devadarta](https://instagram.com/devadarta)

## License
Use como quiser

## Sobre Rosália Benvegnú
[instagram.com/rosaliafotografia](https://instagram.com/rosaliafotografia)

<!-- ## Melhorias e ideias -->

<!-- TODO: Acrescentar referências Youtube e outros sites de estudo
 -->
